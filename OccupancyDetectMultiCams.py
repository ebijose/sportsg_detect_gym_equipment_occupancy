# ============================================================================
# Copyright (c) 2019 Government Technology Agency. All Rights Reserved.

# This software is the confidential and proprietary information of GovTech, SIOT
# ("Confidential Information"). You shall not disclose such Confidential Information
# and shall use it only in accordance with the terms of the project/license agreement you
# entered into with Government Technology Agency (GovTech).
#
# GOVTECH MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF THE
# SOFTWARE, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
# PURPOSE, OR NON-INFRINGEMENT. GOVTECH SHALL NOT BE LIABLE FOR ANY DAMAGES
# SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
# THIS SOFTWARE OR ITS DERIVATIVES.
# ============================================================================
#
# OccupancyDetectMultiCams.py shows how to capture images from multiple cameras
# simultaneously and process images to find out occupancies at user selected ROIs.
# It relies on information provided in the Enumeration, Acquisition, and NodeMapInfo examples.
#
# Loop and vectors are used to allow for simultaneous acquisitions of FLIR Blackfly cameras.

import os
import PySpin
import cv2
import time
import json
import pickle
import numpy as np
from urllib.request import Request, urlopen
from urllib.error import URLError

drawing = False  # true if mouse is pressed
ix, iy = -1, -1
global x, y

conditionsSetURL = "https://smartgym.siotgov.tech/v0.1/machines/bulk_set_availability"


def upload_data(data):
    params = json.dumps(data).encode('utf8')
    req = Request(conditionsSetURL, data=params, headers={'content-type': 'application/json', 'X-API-Key': 'tgif'}, method='PUT')
    # for more info on catching URLError, please read https://docs.python.org/3.4/howto/urllib2.html
    try:
        response = urlopen(req)
        print('response = ', response)
        print("Data uploaded.")
    except URLError as e:
        if hasattr(e, 'reason'):
            print('Failed to reach the data server. Reason: %', e.reason)
        elif hasattr(e, 'code'):
            print('The server couldn\'t fulfill the request. Error code: %d', e.code)


# mouse callback function
def select_regions(event, x, y, flags, param):
    global ix, iy, drawing

    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        ix, iy = x, y

    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing:
            cv2.rectangle(param[0], (ix, iy), (x, y), (255, 0, 0), 4)

    elif event == cv2.EVENT_LBUTTONUP:
        drawing = False
        cv2.rectangle(param[0], (ix, iy), (x, y), (255, 255, 255), 4)

        with open(param[1], 'ab') as fp:
            pickle.dump(np.array([ix, iy, x, y]), fp)


def read_roi_file(fileName):
    if os.path.isfile(fileName):
        objects = []
        with (open(fileName, "rb")) as openfile:
            while True:
                try:
                    objects.append(pickle.load(openfile))
                except EOFError:
                    break

        print('read pickled file ', objects)
        print('no of regions = ', len(objects))
        return objects
    else:
        return []


def acquire_images(cam_list):
    """
    This function acquires and images from each device.

    :param cam_list: List of cameras
    :type cam_list: CameraList
    :return: True if successful, False otherwise.
    :rtype: bool
    """

    print('*** IMAGE ACQUISITION ***\n')
    try:
        result = True

        # Prepare each camera to acquire images
        #
        # *** NOTES ***
        # For pseudo-simultaneous streaming, each camera is prepared as if it
        # were just one, but in a loop. Notice that cameras are selected with
        # an index. We demonstrate pseduo-simultaneous streaming because true
        # simultaneous streaming would require multiple process or threads,
        # which is too complex for an example. 
        #
        # Serial numbers are the only persistent objects we gather in this
        # example, which is why a vector is created.
        for i, cam in enumerate(cam_list):

            # Set acquisition mode to continuous
            node_acquisition_mode = PySpin.CEnumerationPtr(cam.GetNodeMap().GetNode('AcquisitionMode'))
            if not PySpin.IsAvailable(node_acquisition_mode) or not PySpin.IsWritable(node_acquisition_mode):
                print('Unable to set acquisition mode to continuous (node retrieval; camera %d). Aborting... \n' % i)
                return False

            node_acquisition_mode_continuous = node_acquisition_mode.GetEntryByName('Continuous')
            if not PySpin.IsAvailable(node_acquisition_mode_continuous) or not PySpin.IsReadable(
                    node_acquisition_mode_continuous):
                print('Unable to set acquisition mode to continuous (entry \'continuous\' retrieval %d). \
                Aborting... \n' % i)
                return False

            acquisition_mode_continuous = node_acquisition_mode_continuous.GetValue()

            node_acquisition_mode.SetIntValue(acquisition_mode_continuous)

            print('Camera %d acquisition mode set to continuous...' % i)

            # Begin acquiring images
            cam.BeginAcquisition()

            print('Camera %d started acquiring images...' % i)

            # Retrieve device serial number for filename0
            node_device_serial_number = PySpin.CStringPtr(cam.GetTLDeviceNodeMap().GetNode('DeviceSerialNumber'))

            if PySpin.IsAvailable(node_device_serial_number) and PySpin.IsReadable(node_device_serial_number):
                device_serial_number = node_device_serial_number.GetValue()
                print('Camera %d serial number set to %s...' % (i, device_serial_number))

            print()

        objects1 = read_roi_file('roi_1')
        objects2 = read_roi_file('roi_2')

        if len(objects1):
            flagRun1 = True
            backSub1 = {}
            for inc in range(len(objects1)):
                backSub1[inc] = cv2.createBackgroundSubtractorMOG2()
        else:
            flagRun1 = False

        if len(objects2):
            flagRun2 = True
            backSub2 = {}
            for inc in range(len(objects2)):
                backSub2[inc] = cv2.createBackgroundSubtractorMOG2()
        else:
            flagRun2 = False

#        mask1 = np.zeros((1536, 2048, 3), np.uint8)
#        mask2 = np.zeros((1536, 2048, 3), np.uint8)

#        for cnt, objects in enumerate(objects1):
#            print('cnt , objects = ', cnt, objects)
#            mask1[objects[0]:objects[2], objects[1]:objects[3]] = 1


#        if len(objects1) and flagRun1 == True:
#            avgIntensity1 = []

#        if len(objects2) and flagRun2 == True:
#            avgIntensity2 = []

        #        for cnt in range(len(objects2)):
        #            print('objects2 = ', objects2[cnt])

        counter = 0
        data = []

        # Retrieve, convert, and save images for each camera
        #
        # *** NOTES ***
        # In order to work with simultaneous camera streams, nested loops are
        # needed. It is important that the inner loop be the one iterating
        # through the cameras; otherwise, all images will be grabbed from a
        # single camera before grabbing any images from another.
        while True:
            try:
                machineCounter = 0
                data = []
                # Retrieve next received image and ensure image completion
                image_result_1 = cam_list[0].GetNextImage()
                image_result_2 = cam_list[1].GetNextImage()

                if image_result_1.IsIncomplete():
                    print('Image incomplete with image status %d ... \n' % image_result_1.GetImageStatus())
                else:
                    # Convert image to BGR8
                    image_converted_1 = image_result_1.Convert(PySpin.PixelFormat_BGR8, PySpin.HQ_LINEAR)

                    #  Convert the Image object to RGB array, returns a 3-D NumPy array of integers
                    img1 = image_converted_1.GetNDArray()

                    if len(objects1) and flagRun1 == True:
                        avgIntensity1 = []

                        for inc in range(len(objects1)):

                            avgIntensity1.append(np.mean(backSub1[inc].apply(img1[objects1[inc][1]:objects1[inc][3],
                                                                             objects1[inc][0]:objects1[inc][2]])))

                            if avgIntensity1[inc] > 2.0:
                                if __debug__:
                                    cv2.namedWindow('ROI 1-' + str(inc + 1), cv2.WINDOW_NORMAL)
                                    cv2.imshow('ROI 1-' + str(inc + 1), img1[objects1[inc][1]:objects1[inc][3],
                                                                 objects1[inc][0]:objects1[inc][2]])
                                data.append({"machineId": "ThreadMill" + (str(machineCounter + 1)), "availability": False})
                                machineCounter += 1
                            else:
                                if __debug__:
                                    cv2.namedWindow('ROI 1-' + str(inc + 1), cv2.WINDOW_NORMAL)
                                    cv2.imshow('ROI 1-' + str(inc + 1),
                                           0 * img1[objects1[inc][1]:objects1[inc][3],
                                               objects1[inc][0]:objects1[inc][2]])
                                data.append({"machineId": "ThreadMill" + (str(machineCounter + 1)), "availability": True})
                                machineCounter += 1

                if image_result_2.IsIncomplete():
                    print('Image incomplete with image status %d ... \n' % image_result_2.GetImageStatus())
                else:
                    # Convert image to BGR8
                    image_converted_2 = image_result_2.Convert(PySpin.PixelFormat_BGR8, PySpin.HQ_LINEAR)

                    #  Convert the Image object to RGB array, returns a 3-D NumPy array of integers
                    img2 = image_converted_2.GetNDArray()

                    if len(objects2) and flagRun2 == True:
                        avgIntensity2 = []

                        for inc in range(len(objects2)):

                            avgIntensity2.append(np.mean(backSub2[inc].apply(img2[objects2[inc][1]:objects2[inc][3],
                                                                             objects2[inc][0]:objects2[inc][2]])))

                            if avgIntensity2[inc] > 2.0:
                                if __debug__:
                                    cv2.namedWindow('ROI 2-' + str(inc + 1), cv2.WINDOW_NORMAL)
                                    cv2.imshow('ROI 2-' + str(inc + 1), img2[objects2[inc][1]:objects2[inc][3],
                                                                 objects2[inc][0]:objects2[inc][2]])
                                data.append({"machineId": "ThreadMill" + (str(machineCounter + 1)), "availability": False})
                                machineCounter += 1
                            else:
                                if __debug__:
                                    cv2.namedWindow('ROI 2-' + str(inc + 1), cv2.WINDOW_NORMAL)
                                    cv2.imshow('ROI 2-' + str(inc + 1),
                                           0 * img2[objects2[inc][1]:objects2[inc][3],
                                               objects2[inc][0]:objects2[inc][2]])
                                data.append({"machineId": "ThreadMill" + (str(machineCounter + 1)), "availability": True})
                                machineCounter += 1

                counter += 1

                if not counter % 10:
                    print('counter = ', counter)

                if counter > 50:
                    if flagRun1 or flagRun2:
                        print('data = ', data)
                        upload_data(data)
                    counter = 0

                if __debug__:
                    cv2.namedWindow("First", cv2.WINDOW_NORMAL)
                    cv2.imshow('First', img1)
                    cv2.namedWindow("Second", cv2.WINDOW_NORMAL)
                    cv2.imshow('Second', img2)

                k = cv2.waitKey(1) & 0xFF
                if k == ord('f'):
                    print('select regions')
                    flagRun1 = False
                    flagRun2 = False
                    param = [img1, 'roi_1']
                    cv2.setMouseCallback('First', select_regions, param)
                elif k == ord('s'):
                    print('select regions')
                    flagRun1 = False
                    flagRun2 = False
                    param = [img2, 'roi_2']
                    cv2.setMouseCallback('Second', select_regions, param)
                elif k == ord('d'):
                    print('removing roi file')
                    flagRun1 = True
                    flagRun2 = True
                    os.remove("roi_file")
                    print("File Removed!, quitting program")
                    break
                elif k == ord('q'):
                    print('quitting')
                    break

                # Release image
                image_result_1.Release()
                image_result_2.Release()

            except PySpin.SpinnakerException as ex:
                print('Error: %s' % ex)
                result = False

        # End acquisition for each camera
        #
        # *** NOTES ***
        # Notice that what is usually a one-step process is now two steps
        # because of the additional step of selecting the camera. It is worth
        # repeating that camera selection needs to be done once per loop.
        #
        # It is possible to interact with cameras through the camera list with
        # GetByIndex(); this is an alternative to retrieving cameras as
        # CameraPtr objects that can be quick and easy for small tasks.
        for cam in cam_list:
            # End acquisition
            cam.EndAcquisition()

    except PySpin.SpinnakerException as ex:
        print('Error: %s' % ex)
        result = False

    return result


def print_device_info(nodemap, cam_num):
    """
    This function prints the device information of the camera from the transport
    layer; please see NodeMapInfo example for more in-depth comments on printing
    device information from the nodemap.

    :param nodemap: Transport layer device nodemap.
    :param cam_num: Camera number.
    :type nodemap: INodeMap
    :type cam_num: int
    :returns: True if successful, False otherwise.
    :rtype: bool
    """

    print('Printing device information for camera %d... \n' % cam_num)

    try:
        result = True
        node_device_information = PySpin.CCategoryPtr(nodemap.GetNode('DeviceInformation'))

        if PySpin.IsAvailable(node_device_information) and PySpin.IsReadable(node_device_information):
            features = node_device_information.GetFeatures()
            for feature in features:
                node_feature = PySpin.CValuePtr(feature)
                print('%s: %s' % (node_feature.GetName(),
                                  node_feature.ToString() if PySpin.IsReadable(node_feature) else 'Node not readable'))

        else:
            print('Device control information not available.')
        print()

    except PySpin.SpinnakerException as ex:
        print('Error: %s' % ex)
        return False

    return result


def run_multiple_cameras(cam_list):
    """
    This function acts as the body of the example; please see NodeMapInfo example
    for more in-depth comments on setting up cameras.

    :param cam_list: List of cameras
    :type cam_list: CameraList
    :return: True if successful, False otherwise.
    :rtype: bool
    """
    try:
        result = True

        # Retrieve transport layer nodemaps and print device information for
        # each camera
        # *** NOTES ***
        # This example retrieves information from the transport layer nodemap
        # twice: once to print device information and once to grab the device
        # serial number. Rather than caching the nodem#ap, each nodemap is
        # retrieved both times as needed.
        print('*** DEVICE INFORMATION ***\n')

        for i, cam in enumerate(cam_list):
            # Retrieve TL device nodemap
            nodemap_tldevice = cam.GetTLDeviceNodeMap()

            # Print device information
            result &= print_device_info(nodemap_tldevice, i)

        # Initialize each camera
        #
        # *** NOTES ***
        # You may notice that the steps in this function have more loops with
        # less steps per loop; this contrasts the AcquireImages() function
        # which has less loops but more steps per loop. This is done for
        # demonstrative purposes as both work equally well.
        #
        # *** LATER ***
        # Each camera needs to be deinitialized once all images have been
        # acquired.
        for i, cam in enumerate(cam_list):
            # Initialize camera
            cam.Init()

        # Acquire images on all cameras
        result &= acquire_images(cam_list)

        # Deinitialize each camera
        #
        # *** NOTES ***
        # Again, each camera must be deinitialized separately by first
        # selecting the camera and then deinitializing it.
        for cam in cam_list:
            # Deinitialize camera
            cam.DeInit()

        # Release reference to camera
        # NOTE: Unlike the C++ examples, we cannot rely on pointer objects being automatically
        # cleaned up when going out of scope.
        # The usage of del is preferred to assigning the variable to None.
        del cam

    except PySpin.SpinnakerException as ex:
        print('Error: %s' % ex)
        result = False

    return result


def main():
    """
    Example entry point; please see Enumeration example for more in-depth
    comments on preparing and cleaning up the system.

    :return: True if successful, False otherwise.
    :rtype: bool
    """

    # Since this application saves images in the current folder
    # we must ensure that we have permission to write to this folder.
    # If we do not have permission, fail right away.
    try:
        test_file = open('test.txt', 'w+')
    except IOError:
        print('Unable to write to current directory. Please check permissions.')
        input('Press Enter to exit...')
        return False

    test_file.close()
    os.remove(test_file.name)

    result = True

    # Retrieve singleton reference to system object
    system = PySpin.System.GetInstance()

    # Get current library version
    version = system.GetLibraryVersion()
    print('Library version: %d.%d.%d.%d' % (version.major, version.minor, version.type, version.build))

    # Retrieve list of cameras from the system
    cam_list = system.GetCameras()
    num_cameras = cam_list.GetSize()
    print('Number of cameras detected: %d' % num_cameras)

    # Finish if there are no cameras
    if num_cameras == 0:
        # Clear camera list before releasing system
        cam_list.Clear()

        # Release system instance
        system.ReleaseInstance()

        print('Not enough cameras!')
        input('Done! Press Enter to exit...')
        return False

    # Run program on all cameras
    print('Running program for all cameras...')

    result = run_multiple_cameras(cam_list)

    print('Program complete... \n')

    # Clear camera list before releasing system
    cam_list.Clear()

    # Release system instance
    system.ReleaseInstance()

    input('Done! Press Enter to exit...')
    return result


if __name__ == '__main__':
    main()
