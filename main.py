import os
import cv2
import json
import pickle
import os.path
import numpy as np
import socket                   	# Import socket module
from urllib.request import Request, urlopen
from urllib.error import URLError

import urllib.request
import threading

# https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_gui/py_mouse_handling/py_mouse_handling.html

drawing = False     # true if mouse is pressed
ix, iy = -1, -1
global x, y

conditionsSetURL = "http://10.10.3.9:22090/v0.1/machines/bulk_set_availability"

# s = socket.socket()             # Create a socket object


def upload_data(data):
    params = json.dumps(data).encode('utf8')
    req = Request(conditionsSetURL, data=params, headers={'content-type': 'application/json', 'X-API-Key': 'tgif'}, method='PUT')
    # for more info on catching URLError, please read https://docs.python.org/3.4/howto/urllib2.html
    try:
        response = urlopen(req)
        print('response = ', response)
        print("Data uploaded.")
    except URLError as e:
        if hasattr(e, 'reason'):
            print('Failed to reach the data server. Reason: %', e.reason)
        elif hasattr(e, 'code'):
            print('The server couldn\'t fulfill the request. Error code: %d', e.code)


# mouse callback function
def select_regions(event, x, y, flags, img):
    global ix, iy, drawing

    # f = open("selected_roi.txt", "a+")

    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        ix, iy = x, y

    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing:
            cv2.rectangle(img, (ix, iy), (x, y), (255, 0, 0), 4)

    elif event == cv2.EVENT_LBUTTONUP:
        drawing = False
        cv2.rectangle(img, (ix, iy), (x, y), (255, 255, 255), 4)
#        print('ix iy x y : ', ix, iy, x, y)

        with open('roi_file', 'ab') as fp:
            pickle.dump(np.array([ix, iy, x, y]), fp)


def read_roi_file():
#    print('Does roi file exists? ', os.path.isfile('roi_file'))

    if os.path.isfile('roi_file'):
        objects = []
        with (open("roi_file", "rb")) as openfile:
            while True:
                try:
                    objects.append(pickle.load(openfile))
                except EOFError:
                    break

#        print('read pickled file ', objects)
#        print('no of regions = ', len(objects))
        return objects
    else:
        return []

def main():
    cap = cv2.VideoCapture(0)
    ret_val, img = cap.read()

#    cv2.imshow('image', img)

    mask = np.zeros(img.shape, np.uint8)

    objects = read_roi_file()

    if len(objects):
        flagRun = True
        backSub = {}
        for inc in range(len(objects)):
            backSub[inc] = cv2.createBackgroundSubtractorMOG2()
    else:
        flagRun = False

    counter = 0

    while True:
        counter += 1
        ret_val, img = cap.read()

#        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        objects = read_roi_file()

        if len(objects):
            for inc in range(len(objects)):
                cv2.rectangle(img, (objects[inc][0], objects[inc][1]), (objects[inc][2], objects[inc][3]),
                      (255, 255, 255), 4)

        if len(objects) and flagRun == True:
#            backSub = {}
            avgIntensity = []
            data = []
            for inc in range(len(objects)):
#                backSub[inc] = cv2.createBackgroundSubtractorMOG2()
#                print('count backSub = ', inc)

                cv2.rectangle(mask, (objects[inc][0], objects[inc][1]), (objects[inc][2], objects[inc][3]),
                              (255, 255, 255), 4)
                mask[objects[inc][1]:objects[inc][3], objects[inc][0]:objects[inc][2]] = \
                    img[objects[inc][1]:objects[inc][3], objects[inc][0]:objects[inc][2]]

                avgIntensity.append(np.mean(backSub[inc].apply(img[objects[inc][1]:objects[inc][3],
                                                               objects[inc][0]:objects[inc][2]])))
#                print('avg Intensity ', inc, avgIntensity[inc])

                if avgIntensity[inc] > 2.0:
                    cv2.imshow('cropped'+str(inc), img[objects[inc][1]:objects[inc][3],
                                                   objects[inc][0]:objects[inc][2]])
                    data.append({"machineId": "ThreadMill"+(str(inc+1)), "availability": False})
                else:
                    cv2.imshow('cropped' + str(inc),
                               0 * img[objects[inc][1]:objects[inc][3], objects[inc][0]:objects[inc][2]])
                    data.append({"machineId": "ThreadMill" + (str(inc + 1)), "availability": True})

        if counter > 50:
            #data = [{"machineId": "T01", "availability": False}, {"machineId": "T02", "availability": False}]
            print('data = ', data)
            upload_data(data)
            counter = 0

        print('counter = ', counter)

        cv2.imshow('image', img)
        cv2.imshow('result', mask)

        k = cv2.waitKey(1) & 0xFF
        if k == ord('s'):
            print('select regions')
            flagRun = False
            cv2.setMouseCallback('image', select_regions, img)
        elif k == ord('d'):
            print('detect occupancy')
            flagRun = True
            os.remove("roi_file")
            print("File Removed!, quitting program")
            break
        elif k == ord('q'):
            print('quitting')
            break

    cv2.destroyAllWindows()


if __name__ == '__main__':
    main()